
const searchData = {
    arrYandex: {
        name: 'yandex',
        data: []
    },
    arrGoogle: {
        name: 'google',
        data: []
    },
    arrMail: {
        name: 'mail',
        data: []
    }
}

//слушатель сообщения от главной кнопки с popup страницы
chrome.runtime.onMessage.addListener(
    function (request) {
        if (request.start === "start") {
            chrome.tabs.create({url: "https://yandex.ru/"});
            chrome.runtime.onMessage.addListener(
                function listenerPage(request, sender, sendResponse){
                    if (request.created === "https://yandex.ru/"){
                        sendResponse({platform: "yandex"});
                    }else if (request.created === "https://www.google.com/"){
                        sendResponse({platform: "google"});
                    }else if (request.created === "https://mail.ru/"){
                        sendResponse({platform: "mail"});
                    }else if (request.platform === 'yandex'){
                        searchData.arrYandex.data = request.arrTitle;
                        chrome.tabs.create({url: "https://google.com/"})
                    }else if (request.platform === 'google'){
                        searchData.arrGoogle.data = request.arrTitle;
                        chrome.tabs.create({url: "https://mail.ru/"})
                    }else if (request.platform === 'mail'){
                        searchData.arrMail.data = request.arrTitle;
                        chrome.tabs.create({ url: chrome.runtime.getURL("backgroundPage.html")});
                        chrome.runtime.onMessage.removeListener(listenerPage);
                    }else if (request.created.search(new RegExp('https://yandex.ru/search')) != -1) {
                        sendResponse({parse: "yandex"});
                    }else if (request.created.search(new RegExp('https://www.google.com/search')) != -1) {
                        sendResponse({parse: "google"});
                    }else if (request.created.search(new RegExp('https://go.mail.ru/search')) != -1) {
                        sendResponse({parse: "mail"});
                    }
                }
            )
        }
    }
);


chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.getArr === "all")
            sendResponse({searchData: searchData});
    }
);
