document.addEventListener('DOMContentLoaded', () => {


    chrome.runtime.sendMessage({created: `${window.location.href}`},function(response) {
        if (response.platform === 'yandex') {
            platforms.yandex.fillForm();
        }else if (response.platform === 'google') {
            platforms.google.fillForm();
        }else if (response.platform === 'mail') {
            platforms.mail.fillForm();
        }else if (response.parse === 'yandex') {
            platforms.yandex.startParse();
        }else if (response.parse === 'google') {
            platforms.google.startParse();
        }else if (response.parse === 'mail') {
            platforms.mail.startParse();
        }
    })

    class Platform {
        constructor(namePlatform, inputSel, btnSel, titleSel) {
            this.namePlatform = namePlatform;
            this.inputSel = inputSel;
            this.btnSel = btnSel;
            this.titleSel = titleSel;
            this.arrTitle = [];
            this.searchWord = 'купить лопату';
        }
        fillForm(){
            const input = document.querySelector(this.inputSel);
            const btn = document.querySelector(this.btnSel);
            const commands = [
                new KeyboardEvent('keydown', {bubbles: true}),
                new KeyboardEvent('keypress', {bubbles: true}),
                new KeyboardEvent('keyup', {bubbles: true}),
                new Event('input', {bubbles: true}),
                new Event('change', {bubbles: true})
            ];
            input.value = this.searchWord;
            // перечисляем команды имитации ввода в поле поиска яндекс
            commands.forEach(elem => {
                input.dispatchEvent(elem);
            });

            btn.click();
        }
        startParse(){
            const arrLink = document.querySelectorAll(this.titleSel);
            for (let i = 0; i < 10; i++){
                this.arrTitle.push(arrLink[i].textContent)
            }
            chrome.runtime.sendMessage({platform: this.namePlatform, arrTitle: this.arrTitle})
        }
    }


    const platforms = {
        google: new Platform('google', '.gLFyf','.gNO89b', '.LC20lb' ),
        mail: new Platform('mail', '.search__input','.search__btn', '.SnippetResultTitle-link' ),
        yandex: new Platform('yandex', '.input__control','.button_theme_search', '.OrganicTitleContentSpan' ),
    }







//     googleSelectors: {
//         input: '.gLFyf',
//         btn: '.gNO89b',
//         title: '.LC20lb',
//         arrTitle: [],
//         fillForm(){
//
//
//     },
//     yandexSelectors: {
//         input: '.input__control',
//         btn: '.button_theme_search',
//         title: '.OrganicTitleContentSpan',
//         arrTitle: [],
//         fillForm(){
//             const input = document.querySelector(this.input);
//             const btn = document.querySelector(this.btn);
//             const commands = [
//                 new KeyboardEvent('keydown', {bubbles: true}),
//                 new KeyboardEvent('keypress', {bubbles: true}),
//                 new KeyboardEvent('keyup', {bubbles: true}),
//                 new Event('input', {bubbles: true}),
//                 new Event('change', {bubbles: true})
//             ];
//             input.value = 'купить лопату';
//
//             //перечисляем команды имитации ввода в поле поиска яндекс
//             commands.forEach(elem => {
//                 input.dispatchEvent(elem);
//             });
//
//             btn.click();
//         },
//         startParse(){
//             const arrLink = document.querySelectorAll(this.title);
//             for (let i = 0; i < 10; i++){
//                 platforms.yandexSelectors.arrTitle.push(arrLink[i].innerText)
//             }
//             alert(this.arrTitle);
//             chrome.runtime.sendMessage({arrYandex: this.arrTitle})
//         },
//         seyHello(){
//             alert('hello')
//         }
//     },
//     mailSelectors: {
//         input: '.search__input',
//         btn: '.search__btn',
//         title: '.SnippetResultTitle-link',
//         arrTitle: [],
//         fillForm(){
//             const input = document.querySelector(this.input);
//             const btn = document.querySelector(this.btn);
//             const commands = [
//                 new KeyboardEvent('keydown', {bubbles: true}),
//                 new KeyboardEvent('keypress', {bubbles: true}),
//                 new KeyboardEvent('keyup', {bubbles: true}),
//                 new Event('input', {bubbles: true}),
//                 new Event('change', {bubbles: true})
//             ];
//             input.value = 'купить лопату';
//
//             //перечисляем команды имитации ввода в поле поиска яндекс
//             commands.forEach(elem => {
//                 input.dispatchEvent(elem);
//             });
//
//             btn.click();
//         },
//         startParse(){
//             const arrLink = document.querySelectorAll(this.title);
//             for (let i = 0; i < 10; i++){
//                 this.arrTitle.push(arrLink[i].textContent)
//             }
//             alert(this.arrTitle);
//             chrome.runtime.sendMessage({arrMail: this.arrTitle})
//         },
//     },
// }
})