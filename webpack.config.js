const path = require('path'),
    CopyWebpackPlugin = require('copy-webpack-plugin'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    { VueLoaderPlugin } = require('vue-loader');


const options = {
    entry: {
        popup: path.join(__dirname, 'src','popup', 'popup.js'),
        background: path.join(__dirname, 'src', 'background.js'),
        content: path.join(__dirname, 'src', 'content.js'),
        backgroundPage: path.join(__dirname, 'src','backgroundPage', 'backgroundPage.js')
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name].bundle.js',
        clean: true
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]
            }
        ],
    },

    plugins: [
        new VueLoaderPlugin(),
        new CopyWebpackPlugin({
            patterns: [
                { from: 'src/manifest.json', to: path.join(__dirname, 'dist') },
                { from: 'src/icon.png', to: path.join(__dirname, 'dist') },
            ]
        }),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'src','backgroundPage','backgroundPage.html'),
            filename: 'backgroundPage.html',
            chunks: ['backgroundPage'],
        }),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'src','popup', 'popup.html'),
            filename: 'popup.html',
            chunks: ['popup'],
        }),
    ],
};

module.exports = options;
